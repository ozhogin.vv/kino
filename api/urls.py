from django.urls import path, re_path, include
from .views import *
from rest_framework import routers


router = routers.SimpleRouter()
router.register(r'grades', GradesViewSet)


urlpatterns = [
    path('api/v1/filmslist', FilmsAPIView.as_view()),
    re_path('^api/v1/filmslist/(?P<filmurl>.+)/$', FilmAPIView.as_view()),
    path('api/v1/usergradeslist', UserGradesAPIView.as_view()),
    path('api/v1/', include(router.urls)),
    path('api/v1/gradeslist/<str:filmurl>/', GradesViewSet.as_view({'get': 'get_grades', 'post': 'grade_create'}), name='gradeslist'),
    #path('api/creategrade/<str:filmurl>', views.createGrade),
]