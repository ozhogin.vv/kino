from rest_framework import serializers
from main.models import Films, Grades
from django.db.models import Q
from rest_framework.validators import UniqueValidator

class FilmsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Films
        fields = '__all__'


class GradesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grades
        fields = '__all__'

    def validate(self, data):
        if (Grades.objects.filter(Q(user_id=data['user_id']) & Q(film_name=data['film_name']))):
            raise serializers.ValidationError({'error': 'Вы уже оставляли рецензию'})
        return data