from rest_framework import generics, filters, viewsets
from main.models import Films, Grades
from .serializers import FilmsSerializer, GradesSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from django.shortcuts import redirect
from rest_framework.response import Response


class FilmsAPIView(generics.ListAPIView):
    serializer_class = FilmsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

    def get_queryset(self):
           queryset = Films.objects.all()
           filmurl = self.request.query_params.get('filmurl')
           if filmurl is not None:
               queryset = queryset.filter(url=filmurl)
           return queryset


class FilmAPIView(FilmsAPIView):
    def get_queryset(self):
        filmurl = self.kwargs['filmurl']
        return Films.objects.filter(url=filmurl)


class UserGradesAPIView(generics.ListAPIView):
    serializer_class = GradesSerializer

    def get_queryset(self):
        user = self.request.user.id
        return Grades.objects.filter(user_id=user)


class GradesViewSet(viewsets.ModelViewSet):
    queryset = Grades.objects.all()
    serializer_class = GradesSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['film_name']

    @action(methods=['GET'], detail=True)
    def get_grades(self, request, filmurl):
        user_id = self.request.user.id
        film_name = Films.objects.get(url=filmurl).name
        grades = Grades.objects.filter(user_id=user_id, film_name=film_name)
        serializer = GradesSerializer(grades, many=True)
        return Response(serializer.data)


    @action(methods=['POST'], detail=True)
    def grade_create(self, request, filmurl):
        serializer = GradesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
        return redirect('/movies/' + filmurl)