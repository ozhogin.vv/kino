# Generated by Django 4.0.5 on 2022-07-03 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_persons'),
    ]

    operations = [
        migrations.CreateModel(
            name='Experts',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('img', models.CharField(default='', max_length=100, verbose_name='img')),
                ('rating', models.CharField(default='', max_length=100, verbose_name='rating')),
            ],
            options={
                'verbose_name': 'Эксперт',
                'verbose_name_plural': 'Эксперты',
            },
        ),
        migrations.CreateModel(
            name='ExpertsFilms',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('film_id', models.IntegerField(default=0, max_length=10, verbose_name='film_id')),
                ('expert_id', models.IntegerField(default=0, max_length=10, verbose_name='expert_id')),
            ],
            options={
                'verbose_name': 'Эксперт-Фильм',
                'verbose_name_plural': 'Эксперты-Фильмы',
            },
        ),
        migrations.CreateModel(
            name='PersonsFilms',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('film_id', models.IntegerField(default=0, max_length=10, verbose_name='film_id')),
                ('person_id', models.IntegerField(default=0, max_length=10, verbose_name='person_id')),
            ],
            options={
                'verbose_name': 'Персона-Фильм',
                'verbose_name_plural': 'Персоны-Фильмы',
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.CharField(default='', max_length=100, verbose_name='login')),
                ('password', models.CharField(default='', max_length=100, verbose_name='password')),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
            },
        ),
        migrations.AddField(
            model_name='films',
            name='discription',
            field=models.CharField(default='', max_length=9999, verbose_name='discription'),
        ),
        migrations.AddField(
            model_name='persons',
            name='discription',
            field=models.CharField(default='', max_length=9999, verbose_name='discription'),
        ),
    ]
