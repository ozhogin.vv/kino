# Generated by Django 4.0.5 on 2023-01-21 16:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_remove_films_group_remove_films_person_films_groups_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='films',
            old_name='groups',
            new_name='group',
        ),
        migrations.RenameField(
            model_name='films',
            old_name='persons',
            new_name='person',
        ),
    ]
