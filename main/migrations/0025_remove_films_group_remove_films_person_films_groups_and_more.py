# Generated by Django 4.0.5 on 2023-01-21 13:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0024_remove_films_groups_remove_films_persons_films_group_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='films',
            name='group',
        ),
        migrations.RemoveField(
            model_name='films',
            name='person',
        ),
        migrations.AddField(
            model_name='films',
            name='groups',
            field=models.ManyToManyField(to='main.groups', verbose_name='Группы'),
        ),
        migrations.AddField(
            model_name='films',
            name='persons',
            field=models.ManyToManyField(to='main.persons', verbose_name='Персоны'),
        ),
    ]
