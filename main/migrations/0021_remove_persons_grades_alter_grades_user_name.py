# Generated by Django 4.0.5 on 2023-01-20 13:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_films_groups_films_persons_persons_grades_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='persons',
            name='grades',
        ),
        migrations.AlterField(
            model_name='grades',
            name='user_name',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='main.users'),
        ),
    ]
