from django.db import models
from simple_history.models import HistoricalRecords


class Persons(models.Model):
    PERSONS_GROUP_CHOICES = [
        ('actor', 'actor'),
        ('director', 'director'),
    ]
    name = models.CharField('Имя', max_length=100)
    img = models.CharField('Фото', max_length=100, default='')
    group = models.CharField('Группа', max_length=100, default='actor', choices=PERSONS_GROUP_CHOICES)
    country = models.CharField('Страна', max_length=100, default='')
    birthdate = models.DateField('Дата рождения', max_length=100, default=0)
    genres = models.CharField('Жанры', max_length=100, default='')
    filmscount = models.IntegerField('Количество фильмов', max_length=4, default=0)
    discription = models.CharField('Описание', max_length=9999, default='')
    url = models.CharField('url', max_length=100, default='')
    history = HistoricalRecords()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Персона'
        verbose_name_plural = 'Персоны'

    def get_person(self, personurl):
        return self.objects.filter(url=personurl)[0]

    def get_persons(self, filmurl):
        film = Films.objects.filter(url=filmurl)[0]
        return self.objects.filter(film=film.id)


class Groups(models.Model):
    group = models.CharField('группа', max_length=100, default='best')
    history = HistoricalRecords()

    def __str__(self):
        return self.group

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'


class Films(models.Model):
    CATEGORY_CHOICES = [
        ('film', 'film'),
        ('serial', 'serial'),
    ]
    name = models.CharField('название', max_length=100)
    img = models.CharField('фото', max_length=100, default='')
    category = models.CharField('категория', max_length=100, default='film', choices=CATEGORY_CHOICES)
    discription = models.CharField('описание', max_length=9999, default='')
    url = models.CharField('url', max_length=100, default='')
    person = models.ManyToManyField(Persons, verbose_name='Персоны', related_name='film')
    group = models.ManyToManyField(Groups, verbose_name='Группы')
    history = HistoricalRecords()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'

    def get_film(self, filmurl):
        return self.objects.filter(url=filmurl)[0]

    def get_films(self, personurl):
        person = Persons.objects.filter(url=personurl)[0]
        return self.objects.filter(person=person.id)


class Users(models.Model):
    ROLE_CHOICES = [
        ('admin', 'admin'),
        ('writer', 'writer'),
        ('reader', 'reader'),
    ]
    login = models.CharField('имя', max_length=100, default='')
    password = models.CharField('пароль', max_length=100, default='')
    role = models.CharField('роль', max_length=100, default='', choices=ROLE_CHOICES)
    history = HistoricalRecords()

    def __str__(self):
        return self.login

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Experts(models.Model):
    expert_name = models.OneToOneField(Users, on_delete=models.PROTECT, null=True, verbose_name='Имя эксперта')
    rating = models.IntegerField('рейтинг', max_length=100, default=0)
    articles_count = models.IntegerField('количество рецензий', max_length=100, default=0)
    comments_count = models.IntegerField('количество комментариев', max_length=100, default=0)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.expert_name)

    class Meta:
        verbose_name = 'Эксперт'
        verbose_name_plural = 'Эксперты'


class Grades(models.Model):
    user_id = models.ForeignKey(Experts, on_delete=models.PROTECT, null=True, verbose_name='Имя рецензента')
    film_name = models.CharField('Название фильма', max_length=100)
    title = models.CharField('Заголовок', max_length=100, unique=True)
    num = models.IntegerField('Оценка', max_length=100)
    description = models.CharField('описание', max_length=500)
    date = models.DateField('дата создания', null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Оценка'
        verbose_name_plural = 'Оценки'

    def get_grades(self, filmurl):
        film = Films.objects.filter(url=filmurl)[0]
        return self.objects.filter(film_name=film.name)