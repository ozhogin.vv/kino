from django.urls import path, include
from . import views
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', views.index, name='index'),
    path('serials', views.serials, name='serials'),
    path('actors', views.actors, name='actors'),
    path('directors', views.directors, name='directors'),
    path('grades', views.grades, name='grades'),
    path('movies/<str:filmurl>', views.film, name='film'),
    path('persons/<str:personurl>', views.person, name='person'),
    path('creategrade/<str:filmurl>', views.createGrade, name='createGrade'),
    path('editgrade/<str:filmurl>/<int:gradeid>', views.editGrade, name='editGrade'),
    path('deletegrade/<str:filmurl>/<int:gradeid>', views.deleteGrade, name='deleteGrade'),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]