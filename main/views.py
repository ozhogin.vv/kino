from django.shortcuts import render, redirect
from .models import Films
from .models import Persons
from .models import Grades
from .models import Experts


def index(request):
    films = Films.objects.all()
    best_films = Films.objects.filter(group=1)
    recommend_films = Films.objects.filter(group=2)
    new_films = Films.objects.filter(group=3)
    title = 'Фильмы'
    category = 'film'
    return render(request, 'main/index.html', {'films': films, 'title': title, 'category': category, 'best_films': best_films, 'recommend_films': recommend_films, 'new_films': new_films})


def serials(request):
    films = Films.objects.all()
    best_films = Films.objects.filter(group=1)
    recommend_films = Films.objects.filter(group=2)
    new_films = Films.objects.filter(group=3)
    title = 'Сериалы'
    category = 'serial'
    return render(request, 'main/index.html', {'films': films, 'title': title, 'category': category, 'best_films': best_films, 'recommend_films': recommend_films, 'new_films': new_films})


def actors(request):
    persons = Persons.objects.all()
    group = 'actor'
    title = 'Актеры'
    return render(request, 'main/actors-list.html', {'persons': persons, 'group': group, 'title': title})


def directors(request):
    persons = Persons.objects.all()
    group = 'director'
    title = 'Режиссеры'
    return render(request, 'main/actors-list.html', {'persons': persons, 'group': group, 'title': title})


def film(request, filmurl):
    film = Films.get_film(Films, filmurl)
    persons = Persons.get_persons(Persons, filmurl)
    grades = Grades.get_grades(Grades, filmurl)
    return render(request, 'main/film.html', {'persons': persons, 'film': film, 'grades': grades})


def person(request, personurl):
    person = Persons.get_person(Persons, personurl)
    films = Films.get_films(Films, personurl)
    return render(request, 'main/actor.html', {'films': films, 'person': person})


def grades(request):
    return render(request, 'main/grades.html')


def createGrade(request, filmurl):
    grade = Grades()
    grade.user_id = Experts.objects.filter(id=4)[0]
    grade.film_name = request.POST.get("film_name")
    grade.title = request.POST.get("title")
    grade.num = request.POST.get("num")
    grade.description = request.POST.get("description")
    grade.save()
    return redirect('/movies/' + filmurl)


def editGrade(request, filmurl, gradeid):
    grade = Grades.objects.get(id=gradeid)
    grade.film_name = request.POST.get("film_name")
    grade.title = request.POST.get("title")
    grade.num = request.POST.get("num")
    grade.description = request.POST.get("description")
    grade.save()
    return redirect('/movies/' + filmurl)


def deleteGrade(request, filmurl, gradeid):
    grade = Grades.objects.get(id=gradeid)
    grade.delete()
    return redirect('/movies/' + filmurl)