from django.contrib import admin
from .models import Films, Persons, Users, Groups, Grades, Experts
from django.db.models import QuerySet
from django.utils.safestring import mark_safe
from import_export.admin import ImportExportModelAdmin
from simple_history.admin import SimpleHistoryAdmin


class FilmsAdmin(SimpleHistoryAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['name', 'category', 'get_html_photo']
    list_editable = ['category']
    ordering = ['name']
    list_per_page = 7
    actions = ['set_category_film', 'set_category_serial']
    search_fields = ['name__istartswith']
    readonly_fields = ['get_html_photo']
    list_filter = ['category']
    filter_horizontal = ['person', 'group']

    def get_html_photo(self, object):
        if object.img:
            return mark_safe(f'<img src="/static/{object.img}" width=100>')

    get_html_photo.short_description = "Изображение"

    @admin.action(description='Установить категорию "film"')
    def set_category_film(self, request, queryset: QuerySet):
        count_updated = queryset.update(category='film')
        self.message_user(
            request,
            f'Было обновлено {count_updated} записей'
        )

    @admin.action(description='Установить категорию "serial"')
    def set_category_serial(self, request, queryset: QuerySet):
        count_updated = queryset.update(category='serial')
        self.message_user(
            request,
            f'Было обновлено {count_updated} записей'
        )


class PersonsAdmin(SimpleHistoryAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['name', 'group', 'country', 'birthdate', 'filmscount', 'get_html_photo']
    list_editable = ['group', 'filmscount']
    ordering = ['name']
    list_per_page = 7
    actions = ['set_group_actor', 'set_group_director']
    search_fields = ['name__istartswith']
    readonly_fields = ['get_html_photo']
    list_filter = ['country', 'group']

    def get_html_photo(self, object):
        if object.img:
            return mark_safe(f'<img src="/static/{object.img}" width=50>')

    get_html_photo.short_description = "Фото"

    @admin.action(description='Установить группу "actor"')
    def set_group_actor(self, request, queryset: QuerySet):
        count_updated = queryset.update(group='actor')
        self.message_user(
            request,
            f'Было обновлено {count_updated} записей'
        )

    @admin.action(description='Установить группу "director"')
    def set_group_director(self, request, queryset: QuerySet):
        count_updated = queryset.update(group='director')
        self.message_user(
            request,
            f'Было обновлено {count_updated} записей'
        )


class GradesAdmin(SimpleHistoryAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['title', 'film_name', 'user_id']


class UsersAdmin(SimpleHistoryAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['login', 'role']


class ExpertsAdmin(SimpleHistoryAdmin, ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ['expert_name', 'rating', 'articles_count', 'comments_count']


admin.site.site_title = 'Django Kino'
admin.site.site_header = 'Django Kino'

# Register your models here.

admin.site.register(Films, FilmsAdmin)
admin.site.register(Persons, PersonsAdmin)
admin.site.register(Users, UsersAdmin)
admin.site.register(Groups)
admin.site.register(Grades, GradesAdmin)
admin.site.register(Experts, ExpertsAdmin)

